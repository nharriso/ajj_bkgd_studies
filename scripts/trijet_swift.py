import scripts.config as config
import python.run_anaFit as run_anaFit
import os


# May want to loop over these at some point?
cdir = config.cdir
#channelNames=["BkgLow_2_alpha0_SR1_tagged", "BkgLow_3_alpha0_SR1_tagged"]
#channelNames=["MassOrdered_2"]
channelNames=["PtOrdered6"]
rangeslow=[200, 300, 400, 500, 600, 700]
rangeshigh=[400, 500, 600, 700, 800, 900]
#rangeslow=[200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700]
#rangeshigh=[400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900]
#rangeslow=[200, 350, 500]
#rangeshigh=[500, 650, 800]


#rangeslow = range(200, 700, 10)
#rangeshigh = range(400, 900, 10)
rangeslow = range(700, 900, 10)
rangeshigh = range(900, 1100, 10)


#rangeslow=[200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800]
#rangeshigh=[300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900]

fitNames = ["threePar"]

for rangelow, rangehigh in zip(rangeslow, rangeshigh):
    for fitName in fitNames:
      for channelName in channelNames:
        fitFunction = config.fitFunctions[fitName]["Config"]

        nbkg="1E7,0,1E8"
        topfile=config.samples[channelName]["topfile"]
        categoryfile=config.samples[channelName]["categoryfile"]
        dataFile=config.samples[channelName]["inputFile"]
        datahist=config.samples[channelName]["histname"]
  
        # Output file names, which will be written to outputdir
        wsfile = config.getFileName("FitResult_%s_swift_1GeVBin_GlobalFit"%(fitName), cdir + "/scripts/", channelName, rangelow, rangehigh) + ".root"
        outputfile = config.getFileName("FitResult_%s_swift_bkgonly"%(fitName), cdir + "/scripts/", channelName, rangelow, rangehigh) + ".root"
    
        outputdir = channelName
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)

        run_anaFit.run_anaFit(
               datafile=dataFile, 
               datahist=datahist,
               categoryfile=categoryfile,
               topfile=topfile,
               fitFunction=fitName,
               cdir=cdir ,
               wsfile=wsfile,
               nbkg=nbkg,
               ntoys = 0,
               outdir=outputdir,
               outputstring="%s"%(fitName),
               rangelow=rangelow,
               rangehigh=rangehigh,
               outputfile=outputfile,
               signalfile="Gaussian",
               maskthreshold=-0.01,
               dosignal=0,
               dolimit=0,)
  

